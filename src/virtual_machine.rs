use std::collections::HashMap;

// Op functions are called from the instructions and result is completely mutable. The registers
// and memory are all mutated with no return values.
type OpFunction = fn(vm: &mut VirtualMachine) -> ();

pub struct VirtualMachine {
    pub screen_x: u8,
    pub screen_y: u8,
    // The gameboy's max ram is 8 kb.
    pub ram: [u16; 8192],
    pub v_ram: [u16; 8192],
    pub rom: [u16; 256],
    pub registers: [u16; 6],
    // Table of cpu instructions for easy look up. We read the second byte of the item in memory
    // and the value is the function pointer that is associated to that instruction.
    pub cpu_instructions: HashMap<u8, OpFunction>,
}

impl VirtualMachine {
    pub fn fill_table() -> HashMap<u8, OpFunction> {
        let mut ops_table: HashMap<u8, OpFunction> = HashMap::new();

        ops_table.insert(0x00, no_op);
        ops_table.insert(0x01, load_bc);
        ops_table.insert(0x02, load_reg_a_bc);
        ops_table.insert(0x03, inc_bc);
        ops_table.insert(0x04, inc_b);
        ops_table.insert(0x05, dec_b);
        /*
        ops_table.insert(0x06, ld_b);
        ops_table.insert(0x07, rlca);
        ops_table.insert(0x08, ldsp);
        ops_table.insert(0x09, add_hl_bc);
        ops_table.insert(0x0A, ld_a_bc);
        ops_table.insert(0x0B, dec_bc);
        ops_table.insert(0x0C, inc_c);
        ops_table.insert(0x0D, dec_c);
        ops_table.insert(0x0E, ld_c_d8);
        ops_table.insert(0x0F, rrca);
        */
        ops_table.insert(0x10, stop);

        ops_table
    }

    pub fn new() -> VirtualMachine {
        VirtualMachine {
            screen_x: 160,
            screen_y: 144,
            ram: [0; 8192],
            v_ram: [0; 8192],
            rom: [0; 256],
            registers: [0; 6],
            cpu_instructions: VirtualMachine::fill_table(),
        }
    }
}

// Each op code is up to 16 bits or 2 bytes. We read the op code from the fourth nibble,
// the most significant bit.
// Byte size 0 -> 255 == 4 nibbles
// Word size 0 -> 65535 == 2 bytes

fn no_op(vm: &mut VirtualMachine) {
    vm.registers[Register::PC as usize] += 1;
}

fn load_bc(vm: &mut VirtualMachine) {
    // Will need to concat the first and second byte here.
    // Currently only gets the next byte, but BC is a u16 that can hold two bytes.
    vm.registers[Register::BC as usize] = vm.ram[vm.registers[Register::PC as usize] as usize]
        + vm.ram[(vm.registers[Register::PC as usize] + 1) as usize];
}

fn load_reg_a_bc(vm: &mut VirtualMachine) {
    // TODO: Just use the A value here and not the AF value.
    vm.ram[vm.registers[Register::AF as usize] as usize] = vm.registers[Register::BC as usize];
}

fn inc_bc(vm: &mut VirtualMachine) {}

fn inc_b(vm: &mut VirtualMachine) {}

fn dec_b(vm: &mut VirtualMachine) {}

fn stop(vm: &mut VirtualMachine) {}

pub enum Register {
    // Accumulator and flag
    AF = 0,
    BC = 1,
    DE = 2,
    HL = 3,
    // Stack pointer
    SP = 4,
    // Program counter
    PC = 5,
}
