// For our purposes a word is 16 bits, that is, 2 bytes.
// Nibbles go from least to most significant bit, so fourth nibble would be the first left to right.
// and the first nibble is the least significant bit.
const BYTEMASK: u16 = 0x0F;
const FOURTHADDRESS: u16 = 0x0C;
const THIRDADDRESS: u16 = 0x08;
const SECONDADDRESS: u16 = 0x04;
const MAXBYTEMASK: u16 = 0xFF;

struct Word {
    // Max 255 here, 0xFF
    first_nibble: u8,
    second_nibble: u8,
    // Max 65280 here, that is 0xFF00
    third_nibble: u16,
    fourth_nibble: u16,

    // First byte is the first and second nibble together
    first_byte: u16,
    // Second byte is the third and fourth nibble together
    second_byte: u16,

    // Add up nibbles 1 through 3.
    first_three_nibbles: u16,
}

impl Word {
    fn new(instruction: u16) -> Word {
        Word {
            first_nibble: Word::calc_first_nibble(instruction) as u8,
            second_nibble: Word::calc_second_nibble(instruction) as u8,
            third_nibble: Word::calc_third_nibble(instruction),
            fourth_nibble: Word::calc_fourth_nibble(instruction),
            first_byte: Word::calc_first_byte(instruction),
            second_byte: Word::calc_second_byte(instruction),
            first_three_nibbles: Word::calc_first_three_nibbles(instruction),
        }
    }

    fn calc_first_nibble(instruction: u16) -> u16 {
        instruction & BYTEMASK
    }

    fn calc_second_nibble(instruction: u16) -> u16 {
        (instruction >> SECONDADDRESS) & BYTEMASK
    }

    fn calc_third_nibble(instruction: u16) -> u16 {
        (instruction >> THIRDADDRESS) & BYTEMASK
    }

    fn calc_fourth_nibble(instruction: u16) -> u16 {
        (instruction >> FOURTHADDRESS) & BYTEMASK
    }

    fn calc_first_byte(instruction: u16) -> u16 {
        instruction & MAXBYTEMASK
    }

    fn calc_second_byte(instruction: u16) -> u16 {
        instruction >> THIRDADDRESS
    }

    fn calc_first_three_nibbles(instruction: u16) -> u16 {
        let third = Word::calc_third_nibble(instruction) * u16::pow(16,2);
        let second = Word::calc_second_nibble(instruction) * u16::pow(16, 1);
        let first = Word::calc_first_nibble(instruction);
        third + second + first
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INSTRUCTION: u16 = 0x3412;

    #[test]
    fn first_nibble_test() {
        assert_eq!(Word::calc_first_nibble(INSTRUCTION), 0x02);
    }

    #[test]
    fn second_nibble_test() {
        assert_eq!(Word::calc_second_nibble(INSTRUCTION), 0x01);
    }

    #[test]
    fn third_nibble_test() {
        assert_eq!(Word::calc_third_nibble(INSTRUCTION), 0x04);
    }

    #[test]
    fn fourth_nibble_test() {
        assert_eq!(Word::calc_fourth_nibble(INSTRUCTION), 0x03);
    }

    #[test]
    fn word_test() {
        let value = Word::new(INSTRUCTION);
        assert_eq!(value.second_byte, 0x34);
    }

    #[test]
    fn first_byte_test() {
        let value = Word::new(INSTRUCTION);
        assert_eq!(value.first_byte, 0x12);
    }

    #[test]
    fn first_three_nib_test() {
        let value = Word::new(INSTRUCTION);
        assert_eq!(value.first_three_nibbles, 0x0412);
    }
}